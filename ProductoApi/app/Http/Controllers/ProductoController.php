<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Producto;

class ProductoController extends Controller
{
    public function inicio() {
        return Producto::all();
    }

    public function crear(Request $request) {
        $producto = new Producto();
        $producto->nombre = $request->nombre;
        $producto->descripcion = $request->descripcion;
        $producto->precio = $request->precio;
        $producto->stock = $request->stock;
        $producto->imagen = $request->imagen;
        $producto->save();
        return response()->json(
            ['res'=>true,'message'=>'exitoo'],200);
    }

    public function mostrar($id) {
        return Producto::find($id);
    }

    public function actualizar(Request $request) {
        $producto = Producto::find($request->id);
        $producto->nombre = $request->nombre;
        $producto->descripcion = $request->descripcion;
        $producto->precio = $request->precio;
        $producto->stock = $request->stock;
        $producto->imagen = $request->imagen;
        $producto->save();
        return response()->json(
            ['res'=>true,'message'=>'exitoo'],200);
    }

    public function borrar($id) {
        $producto = Producto::find($id);
        $producto->delete();
        return response()->json(
            ['res'=>true,'message'=>'exitoo'],200);
    }
}
