import React from 'react';
import ReactDOM from 'react-dom';
import App from './Components/App';

let container = document.getElementById('root');

ReactDOM.render(<><App/></>, container);