import React,{ useState, useEffect } from 'react';
import { useParams,useHistory } from "react-router-dom";

const CrearProducto = () => {
    const [producto, setProducto] = useState({nombre:"",descripcion:"",precio:"",stock:"",imagen:""});

    const history = useHistory();
    useEffect(async () => {
        await setProducto({nombre:"",descripcion:"",precio:"",stock:"",imagen:""})
    },[]);

    const crearProducto = (e) => {
        e.preventDefault();

        fetch("http://127.0.0.1:8000/api/crear", {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                nombre: producto.nombre,
                descripcion: producto.descripcion,
                precio: producto.precio,
                stock: producto.stock,
                imagen: producto.imagen,
            })
        })
        .then((response) => response.json())
        .then((responseData) => {
            console.log(responseData)
            history.push("/");
        })
    }

    const cambiar = (e) => {
        e.preventDefault();
        
        if(e.target.name == "nombre"){
            setProducto({
                nombre:e.target.value,descripcion:producto.descripcion,precio:producto.precio,
                stock:producto.stock,imagen:producto.imagen
            })
        }
        else if(e.target.name == "descripcion"){
            setProducto({
                nombre:producto.nombre,descripcion:e.target.value,precio:producto.precio,
                stock:producto.stock,imagen:producto.imagen
            })
        }
        else if(e.target.name == "precio"){
            setProducto({
                nombre:producto.nombre,descripcion:producto.descripcion,precio:e.target.value, 
                stock:producto.stock,imagen:producto.imagen
            })
        }
        else if(e.target.name == "stock"){
            setProducto({
                nombre:producto.nombre,descripcion:producto.descripcion,precio:producto.precio,
                stock:e.target.value,imagen:producto.imagen
            })
        }
        else if(e.target.name == "imagen"){
            setProducto({
                nombre:producto.nombre,descripcion:producto.descripcion, precio:producto.precio,
                stock:producto.stock,imagen:e.target.value
            })
        }
    }

    return(
        <div className="container">
            <div className="col-6 border p-5 mb-3">
                <form className="" onSubmit={e => crearProducto(e)}>
                <div class="form-group mb-2">
                    <label for="exampleInputEmail1">Nombre</label>
                    <input type="text" name="nombre" value={producto.nombre} onChange={e=>cambiar(e)} className="form-control"/>
                </div>
                <div class="form-group mb-2">
                    <label for="exampleInputEmail1">Desc</label>
                    <input type="text" name="descripcion" value={producto.descripcion} onChange={e=>cambiar(e)} className="form-control"/>
                </div>
                <div class="form-group mb-2">
                    <label for="exampleInputEmail1">Precio</label>
                    <input type="text" name="precio" value={producto.precio} onChange={e=>cambiar(e)} className="form-control"/>
                </div>
                <div class="form-group mb-2">
                    <label for="exampleInputEmail1">Stock</label>
                    <input type="number" name="stock" value={producto.stock} onChange={e=>cambiar(e)} className="form-control"/>
                </div>
                <div class="form-group mb-2">
                    <label for="exampleInputEmail1">Imagen</label>
                    <input type="text" name="imagen" value={producto.imagen} onChange={e=>cambiar(e)} className="form-control"/>
                </div>
                <div class="form-group">
                    <button className="btn btn-success col-6">Crear</button>
                </div>
                </form>
            </div>
        </div>
    )
}

export default CrearProducto;