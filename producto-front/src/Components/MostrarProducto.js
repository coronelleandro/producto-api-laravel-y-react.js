import React,{ useState, useEffect } from 'react';
import { useParams } from "react-router-dom";

const CrearProducto = () => {
    const [producto, setProducto] = useState({nombre:"",descripcion:"",precio:"",stock:"",imagen:""});
    const {id} = useParams()

    useEffect(async () => {
        await setProducto({nombre:"",descripcion:"",precio:"",stock:"",imagen:""})
        await mostrarProducto(id)
    },[]);

    const mostrarProducto = (id) => {
        fetch('http://127.0.0.1:8000/api/mostrar/'+id+'')
        .then(result=>result.json())
        .then(items=>{
                console.log(items)
                setProducto({
                    id: items.id,nombre: items.nombre,descripcion: items.descripcion,
                    precio: items.precio,stock: items.stock,imagen: items.imagen
                })
            }
        )
    }

    return(
        <div className="container">
            <ul>
                <li>Id : {producto.id}</li>
                <li>Nombre : {producto.nombre}</li>
                <li>Descripcion : {producto.descripcion}</li>
                <li>Precio : {producto.precio}</li>
                <li>Stock : {producto.stock}</li>
                <li>Imagen : {producto.imagen}</li>
            </ul>
        </div>
    )
}

export default CrearProducto;