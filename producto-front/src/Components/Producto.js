import React,{ useState, useEffect } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

const Producto = () => {
    const [productos, setProductos] = useState([]);

    useEffect(async () => {
        await listarProductos()
        //saludar();
    },[]);

    const listarProductos = () => {
        fetch('http://127.0.0.1:8000/api/')
        .then(result=>result.json())
        .then(
            items=>{
                console.log(items);
                setProductos(items);
        })
    }

    const borrarProducto = (e,valor) => {
        e.preventDefault();
        //console.log(valor)
        fetch("http://127.0.0.1:8000/api/borrar/"+valor+"", {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        })
        .then((response) => response.json())
        .then((responseData) => {
            console.log(responseData)
            listarProductos()
        })
    }

    const elemets = productos.map(
                    (element) => { 
                        return <tr key={element.id}> 
                            <td>{element.id}</td> 
                            <td><Link to={`/mostrar/${element.id}`}>{element.nombre}</Link></td> 
                            <td>{element.descripcion}</td> 
                            <td>{element.precio}</td> 
                            <td>{element.stock}</td> 
                            <td><Link to={`/actualizar/${element.id}`} className="btn btn-warning">Actualizar</Link></td> 
                            <td><button className="btn btn-danger" onClick={e=>borrarProducto(e,element.id)} >Borrar</button></td> 
                        </tr> 
                    });
                    

    return( 
        <div className="container">
            <div className="col-12">         
                <table className="table text-center table-bordered">
                    <thead className="thead-dark bg-dark text-light">
                        <tr>
                            <th scope="col">Id</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Descripcion</th>
                            <th scope="col">Precio</th>
                            <th scope="col">Stock</th>
                            <th scope="col">Actualizar</th>
                            <th scope="col">Borrar</th>
                        </tr>
                    </thead>
                    <tbody>
                        {elemets}
                    </tbody>
                </table>
            </div> 
        </div>  
    )
}

export default Producto;