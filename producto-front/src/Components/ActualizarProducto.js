import React,{ useState, useEffect } from 'react';
import { useParams,useHistory } from "react-router-dom";

const ActualizarProducto = () => {
    const [producto, setProducto] = useState({id:"",nombre:"",descripcion:"",precio:"",stock:"",imagen:""});

    const history = useHistory();
    const {id} = useParams();

    useEffect(async () => {
        await mostrarProducto(id)
        //await console.log(id);
    },[]);

    const mostrarProducto = (id) => {
        fetch('http://127.0.0.1:8000/api/mostrar/'+id+'')
        .then(result=>result.json())
        .then(items=>{
                console.log(items)
                setProducto({
                    id: items.id,nombre: items.nombre,descripcion: items.descripcion,
                    precio: items.precio,stock: items.stock,imagen: items.imagen
                })
            }
        )
    }

    const cambiar = (e) => {
        e.preventDefault();
        if(e.target.name == "nombre"){
            setProducto({
                id:producto.id,nombre:e.target.value,descripcion:producto.descripcion,
                precio:producto.precio,stock:producto.stock,imagen:producto.imagen
            })
        }
        else if(e.target.name == "descripcion"){
            setProducto({
                id:producto.id,nombre:producto.nombre,descripcion:e.target.value,
                precio:producto.precio,stock:producto.stock,imagen:producto.imagen
            })
        }
        else if(e.target.name == "precio"){
            setProducto({
                id:producto.id,nombre:producto.nombre,descripcion:producto.descripcion, 
                precio:e.target.value, stock:producto.stock,imagen:producto.imagen
            })
        }
        else if(e.target.name == "stock"){
            setProducto({
                id:producto.id,nombre:producto.nombre,descripcion:producto.descripcion, 
                precio:producto.precio,stock:e.target.value,imagen:producto.imagen
            })
        }
        else if(e.target.name == "imagen"){
            setProducto({
                id:producto.id,nombre:producto.nombre,descripcion:producto.descripcion, 
                precio:producto.precio,stock:producto.stock,imagen:e.target.value
            })
        }
    }

    const actualizar = (e) => {
        e.preventDefault();
        fetch("http://127.0.0.1:8000/api/actualizar", {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                id: producto.id,nombre: producto.nombre,descripcion: producto.descripcion,
                precio: producto.precio,stock: producto.stock,imagen: producto.imagen,
            })
        })
        .then((response) => response.json())
        .then((responseData) => {
            console.log(responseData);
            history.push("/");
        })
    }

    return(
        <div className="container">
            <div className="col-6 border p-5 mb-3">
                <form className="" onSubmit={e => actualizar(e)}>
                <input type="hidden" name="nombre" value={producto.id} onChange={e=>cambiar(e)} className="form-control"/>
                <div class="form-group mb-2">
                    <label for="exampleInputEmail1">Nombre</label>
                    <input type="text" name="nombre" value={producto.nombre} onChange={e=>cambiar(e)} className="form-control"/>
                </div>
                <div class="form-group mb-2">
                    <label for="exampleInputEmail1">Desc</label>
                    <input type="text" name="descripcion" value={producto.descripcion} onChange={e=>cambiar(e)} className="form-control"/>
                </div>
                <div class="form-group mb-2">
                    <label for="exampleInputEmail1">Precio</label>
                    <input type="text" name="precio" value={producto.precio} onChange={e=>cambiar(e)} className="form-control"/>
                </div>
                <div class="form-group mb-2">
                    <label for="exampleInputEmail1">Stock</label>
                    <input type="number" name="stock" value={producto.stock} onChange={e=>cambiar(e)} className="form-control"/>
                </div>
                <div class="form-group mb-2">
                    <label for="exampleInputEmail1">Imagen</label>
                    <input type="text" name="imagen" value={producto.imagen} onChange={e=>cambiar(e)} className="form-control"/>
                </div>
                <div class="form-group">
                    <button className="btn btn-success col-6">Crear</button>
                </div>
                </form>
            </div>
        </div>
    )
}

export default ActualizarProducto;