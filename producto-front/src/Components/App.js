import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import CrearProducto from "./CrearProducto";
import Producto from "./Producto";
import ActualizarProducto from "./ActualizarProducto";
import MostrarProducto from "./MostrarProducto"; 

const App = () => {

  return (
    <Router>
      <div className="col-12 border-bottom mb-5">
        <nav className="container navbar navbar-expand-lg navbar-light bg-light">
          <a className="navbar-brand" href="#">Navbar</a>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div className="navbar-nav">
              <Link className="nav-item nav-link" to="/">Home</Link>
              <Link className="nav-item nav-link" to="/crear">Crear</Link>
            </div>
          </div>
        </nav>
      </div>
      <Switch>
        <Route path="/" exact component={Producto} />
        <Route path="/crear" component={CrearProducto} />
        <Route path="/actualizar/:id" component={ActualizarProducto} />
        <Route path="/mostrar/:id" component={MostrarProducto} />
      </Switch>
      <footer class="page-footer font-small teal mt-5 border-top">
        <div class="footer-copyright text-center py-3">© 2020 Copyright:
          <a href="https://mdbootstrap.com/"> MDBootstrap.com</a>
        </div>
      </footer>
    </Router>
  );
}

export default App;